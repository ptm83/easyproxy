#!/bin/sh

# Manage OnApp Proxy connections 
# Paul Morris / paul@onapp.com

# Set your username here / this is all you need to change
USERNAME="pmorris"

f_ProxySSH () {
        if [ $1 == "uk" ]; then
        SERV_IP="83.170.70.4"
	PORT="2722"
    fi

    if [ $1 == "us" ]; then
        SERV_IP="69.72.249.232"
	PORT="2222"
    fi

    ssh ${USERNAME}@${SERV_IP} -p ${PORT}
}


f_Connect () {
    if [ $1 == "uk" ]; then
	SERV_IP="83.170.70.4"
        PORT="2722"
    fi

    if [ $1 == "us" ]; then
	SERV_IP="69.72.249.232"
        PORT="2222"
    fi

    echo " - Executing OnApp Proxy, Please press UBIKEY when prompted"
    ssh -f -p${PORT} -D 8080 -C -N ${USERNAME}@${SERV_IP}
    echo " -- Complete\n"
}

f_Kill () {
    PID=`ps ax | grep "ssh -f -p2722 -D 8080" | grep -v grep | awk '{print $1}'`
    if kill -9 $PID &>/dev/null; then
        echo " - Process ${PID} killed"
    else
	echo " - No processes running"
    fi
}

f_Menu () {
    QUIT="no"
    while [ ${QUIT} != "yes" ]; do
        echo "1. Connect to UK Proxy"
        echo "2. Connect to US Proxy"
        echo "3. Kill all proxy processes"
	echo "4. SSH to UK Proxy"
	echo "5. SSH to US Proxy"
        echo "6. Quit"
        read CHOICE

        case ${CHOICE} in
	    1) f_Kill; f_Connect uk; exit 0; ;;
	    2) f_Kill; f_Connect us; exit 0; ;;
	    3) f_Kill; exit 0; ;;
	    4) f_ProxySSH uk; exit 0; ;;
	    5) f_ProxySSH us; exit 0; ;;
	    6) QUIT="yes" ;;
	    *) echo "\"${CHOICE}\" is not valid"
            sleep 2 ;;
        esac
    done
}

f_Menu;
